# Moscow sport change log

## Version 1.0.0 under development

- New: Added all page templates
- New: Added Mossport, Raleway and GothamPro fonts
- New: Added jQuery
- New: Added Pug, SCSS, PostCSS
- New: Added Webpack, Babel

## Project init December 25, 2021
