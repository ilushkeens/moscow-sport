(function ($) {
  'use strict'

  // Плавная прокрутка к якорю
  $('.hash').on('click', function (event) {
    event.preventDefault()

    let sectionId = $(this).attr('href'),
        pageTop = $(sectionId).offset().top

    $('html, body').animate({ scrollTop: pageTop }, 600)
  })

  // Фиксированное видео
  let video = $('.childrens__video'),
      scroll = null,
      height = video.offset().top,
      maxHeight = height + 900

  $(window).on('scroll', function () {
    if ($(window).width() >= 1260) {
      if ($(window).scrollTop() > height && $(window).scrollTop() < maxHeight) {
        scroll = window.pageYOffset - height - 394

        video.css('top', scroll)
      }
    }
  })

  // Раскрывающийся список
  $('.faq-list__item').on('click', 'a', function (event) {
    event.preventDefault()

    let link = $(this)

    if (link.hasClass('open')) {
      link.removeClass('open')
    } else {
      link.addClass('open')
    }
  })

  // Изменяет стиль label в форме
  let inputs = document.querySelectorAll('input')

  inputs.forEach(input => {
    input.addEventListener('focusin', () => {
      input.previousElementSibling.classList.add('focused')
    })

    input.addEventListener('focusout', () => {
      if (input.value == '') input.previousElementSibling.classList.remove('focused')
    })
  })

  // Пол
  $('.consent-form__link').on('click', 'a', function (event) {
    event.preventDefault()

    let link = $(this),
        gender = link.attr('title')

    $('.consent-form__link a').removeClass('selected')

    if (link.hasClass('selected')) {
      link.removeClass('selected')
    } else {
      link.addClass('selected')
    }

    $('input[name=gender]').val(gender)
  })

  // Маска для даты рождения
  $('input[name=birthday]').mask('99.99.9999', { autoclear: false })

  // Popup-окна
  function openPopup(url) {
    $('#popup .overlay').fadeIn(300, function () {
      $('html').css({ 'overflow-y': 'hidden', 'overflow-x': 'hidden' })
      $('#popup, #popup .overlay').show()

      $(url).css('display', 'flex').animate({ opacity: 1 }, 300)
    })
  }

  function closePopup() {
    $('#popup .module').animate({ opacity: 0 }, 300, function () {
      history.pushState('', document.title, window.location.pathname)

      $('#popup .overlay').fadeOut(300)
      $('#popup, #popup .overlay, #popup .module').hide()
      $('html').css('overflow-y', 'visible')
    })
  }

  function togglePopup(url) {
    $('#popup .module').hide()
    $(url).css('display', 'flex').animate({ opacity: 1 }, 300)
  }

  let url = decodeURI(window.location.hash),
      hashes = ['schedule', 'consent'] // Активные окна

  // Открывает окно
  $('.open-popup').on('click', function (event) {
    event.preventDefault()

    url = $(this).attr('href')

    openPopup(url)
    event.stopPropagation()
  })

  // ..при загрузке страницы
  $.each(hashes, function (index, value) {
    if (url.replace('#', '') == value.toString()) openPopup(url)
  })

  // ..при другом открытом окне
  $('.toggle-popup').on('click', function (event) {
    event.preventDefault()

    url = $(this).attr('href')

    togglePopup(url)
    event.stopPropagation()
  })

  // Закрывает окно
  $('.close-popup').on('click', function (event) {
    event.preventDefault()

    closePopup()
  })

  // ..при нажатии на клавишу Escape
  $('body').on('keydown', function (event) {
    if (event.code == 'Escape') closePopup()
  })

  // Отправка формы
  $('form').each(function () {
    let form = $(this),
        button = form.find('.button_send')

    // Функция проверки заполнения обязательных полей формы
    function checkInput() {
      form.find('textarea, input').each(function () {
        let field = $(this)

        // Проверяет обязательно ли поле к заполнению
        if (field.attr('required') !== undefined) {
          if (field.val() != '') {
            // Если поле не пустое удаляет класс-указание
            field.removeClass('empty')
          } else {
            // Если поле пустое добавляет класс-указание
            field.addClass('empty')
          }
        }
      })
    }

    // Функция подсветки незаполненных полей
    function lightEmpty() {
      // Добавляет подсветку
      form.find('.empty').addClass('error')

      // ..и через 1.5 секунды её удаляет
      setTimeout(function () {
        form.find('.empty').removeClass('error')
      }, 1500)
    }

    // Проверяет форму в режиме реального времени
    setInterval(function () {
      checkInput()

      // Считает количество незаполненных полей
      let countEmpty = form.find('.empty').length

      // Добавляет условие-тригер к кнопке отправки формы
      if (countEmpty > 0) {
        if (button.hasClass('button_disabled')) {
          return false
        } else {
          button.addClass('button_disabled')
        }
      } else {
        button.removeClass('button_disabled')
      }
    }, 1000)

    // обрабатывает попытку отправки формы
    button.on('click', 'a', function (event) {
      event.preventDefault()

      if (button.hasClass('button_disabled')) {
        // подсвечивает незаполненные поля
        lightEmpty()
        return false
      } else {
        // отправляет форму
        form.on('submit')

        form.find('input').val('')
        form.find('textarea').val('')

        button.addClass('button_disabled')
      }
    })
  })
}(jQuery))
